#from blockchain import blockexplorer


import argparse
import subprocess
import os
from pathlib import Path
import logging

from blockchain import MinimalChain

def export_to_step_with_HE(input_cad_file, he_modification):
	initial_folder = os.getcwd()
	input_cad_file = Path(args.input_cad_file)
	export_step_file = input_cad_file.name + ".stp"
	export_step_filepath = os.path.join(os.getcwd(),  export_step_file)
	exe_HE_folder = os.path.join(os.getcwd(), "..", "HOOPS_Exchange_Publish_2022_U1_Win_VS2015", "HOOPS_Exchange_Publish_2022_U1", "samples", "exchange", "exchangesource", "ImportExport", "x64", "Debug")
	os.chdir(exe_HE_folder)
	export_step = "" if he_modification else "--ExportStep=True"
	modification_cad_file = "--ModifyMetadata=True" if he_modification else ""
	cmd = ["ImportExport.exe", input_cad_file, export_step_filepath, export_step, modification_cad_file]

	p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT, shell=True, cwd=os.getcwd())
	while True:
		line = p.stdout.readline().strip()
		if line:
			logging.info(str(line))
		elif p.poll() is not None:
			break
	os.chdir(initial_folder)

	return export_step_filepath


#-------------------------------------------------------------------------------
def parse_options():
    """Parse the commande line arguments."""
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('--input-cad-file', '-i', dest='input_cad_file',
        help='filepath to the cad file', required=True)

    args = parser.parse_args()
    return args

if __name__ == "__main__":
	
	logging.basicConfig(level=logging.DEBUG)
	args = parse_options()

	minimal_chain = MinimalChain()
	
	step_file_path = export_to_step_with_HE(args.input_cad_file, False)

	with open(step_file_path, 'rb') as cad_file:
		minimal_chain.add_block(cad_file)

	step_file_path = export_to_step_with_HE(args.input_cad_file, True)
	with open(step_file_path, 'rb') as cad_file:
		minimal_chain.add_block(cad_file)

	toto = 6